import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)

export default new Vuex.Store({
	plugins: [createPersistedState()],
	state: {
		notes: [
			{
				id: 1,
				text: 'Test',
				todos: [
					{
						id: 1,
						checked: true,
						text: 'Сходить в магазин'
					},
					{
						id: 2,
						checked: false,
						text: 'Вынести мусор'
					}
				]
			},
		],
		editNote: {}
	},
	mutations: {
		addNote(state, payload) {
			state.notes.push(payload)
		},
		updateNote(state, payload) {
			Vue.set(state.notes, state.notes.findIndex(note => note.id === payload.id), payload)
		},
		removeNote(state, {id}) {
			state.notes = state.notes.filter(note => note.id !== id)
		},
		removeTodo(state, {noteId, id}) {
			const noteIndex = state.notes.findIndex(note => note.id === noteId)
			state.notes[noteIndex].todos = state.notes[noteIndex].todos.filter(todo => todo.id !== id)
		},
		storeEditNote(state, editNote) {
			state.editNote = {...editNote}
		}
	},
	getters: {
		note(state, {id}) {
			return state.notes.filter(note => note.id === id)
		}
	}
})
